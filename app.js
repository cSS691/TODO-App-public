const express = require('express');
const todoController = require('./controllers/todoController');

const app = express();

//setting up template engine
app.set('view engine', 'ejs');

//Invoke controllers
todoController(app);

//static files
app.use(express.static('./public'));

//listeners for the port
app.listen(80);
console.log('you are listening to port 80');
