A Node.js learning project, uses database operations, and REST APIs using Express.js.
The database used is MongoDB and uses mongoose to interface to the DB.

To run this app you need the latest version of Node.js installed and npm installed.

To install clone repository, and then run "npm install",

In app.js file at line 16,the port that listens to the Express server can be changed.

To run the app  execute "node app.js" in the main root directory.

Open 127.0.0.1:<port configured in app.js>/todo or localhost:<port configured in app.js>/todo.